package com.choucair.framework.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.winium.targets.TargetDesktop;

public class CalculadoraPage extends PageObject{

	public static final TargetDesktop BTN_CUATRO = TargetDesktop.the("Click en el botón Cuatro").
			located(By.name("Cuatro"));
	public static final TargetDesktop BTN_MAS = TargetDesktop.the("Click en el botón de Suma").
			located(By.name("Más"));
	public static final TargetDesktop BTN_CINCO = TargetDesktop.the("Click en el botón Cinco").
			located(By.name("Cinco"));
	public static final TargetDesktop BTN_IGUAL = TargetDesktop.the("Click en el botón Igual").
			located(By.name("Es igual a"));
	public static final TargetDesktop CMP_RESULTADO = TargetDesktop.the("Campo de resultado").
			located(By.id("CalculatorResults"));
	

}
