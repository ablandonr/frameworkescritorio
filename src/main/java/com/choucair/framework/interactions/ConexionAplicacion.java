package com.choucair.framework.interactions;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

public class ConexionAplicacion implements Interaction{

	public static void Conexion () {
		DesktopOptions option = new DesktopOptions();
		option.setApplicationPath("C:\\Windows\\System32\\calc.exe");
		try {
			WiniumDriver driver = new WiniumDriver(new URL("http://localhost:9999"), option);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public <T extends Actor> void performAs(T Juan) {
	}
	

}
