package com.choucair.framework.questions;

import com.choucair.framework.ui.CalculadoraPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class EnElCampoDeResultado implements Question<String>{

	@Override
	public String answeredBy(Actor Juan) {
		return CalculadoraPage.CMP_RESULTADO.resolve().getAttribute("Name");
	}

	public static EnElCampoDeResultado deOperacion() {
		return new EnElCampoDeResultado();
	}

}
