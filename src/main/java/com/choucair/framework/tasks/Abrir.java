package com.choucair.framework.tasks;

import com.choucair.framework.interactions.ConexionAplicacion;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

public class Abrir implements Task{
	
	@Override
	public <T extends Actor> void performAs(T Juan) {
		ConexionAplicacion.Conexion();
	}

	public static Abrir laCalculadora() {
		return Tasks.instrumented(Abrir.class);
	}

}
