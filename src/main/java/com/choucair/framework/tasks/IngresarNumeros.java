package com.choucair.framework.tasks;

import com.choucair.framework.ui.CalculadoraPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.winium.actions.ClickDesk;

public class IngresarNumeros implements Task{

	@Override
	public <T extends Actor> void performAs(T Juan) {
		Juan.attemptsTo(ClickDesk.on(CalculadoraPage.BTN_CUATRO));
		Juan.attemptsTo(ClickDesk.on(CalculadoraPage.BTN_MAS));
		Juan.attemptsTo(ClickDesk.on(CalculadoraPage.BTN_CINCO));
		Juan.attemptsTo(ClickDesk.on(CalculadoraPage.BTN_IGUAL));
	}

	public static IngresarNumeros paraSumarlos() {
		return Tasks.instrumented(IngresarNumeros.class);
	}

}
