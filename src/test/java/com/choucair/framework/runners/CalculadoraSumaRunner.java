package com.choucair.framework.runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions (
		features = "src/test/resources/features/CalculadoraSumaDosNumeros.feature",
		tags= "@SumaDeDosNumeros",
		glue="com.choucair.framework.stepdefinitions", 
		snippets= SnippetType.CAMELCASE)

public class CalculadoraSumaRunner {

}