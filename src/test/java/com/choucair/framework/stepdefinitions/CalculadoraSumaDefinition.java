package com.choucair.framework.stepdefinitions;

import com.choucair.framework.questions.EnElCampoDeResultado;
import com.choucair.framework.tasks.Abrir;
import com.choucair.framework.tasks.IngresarNumeros;

import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.winium.abilities.Use;
import net.thucydides.core.annotations.Managed;

import static org.hamcrest.Matchers.equalTo;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

public class CalculadoraSumaDefinition {

    @Managed
    private Actor Juan = Actor.named("Juan");
    
    @Before
	public void configuracionInicial() {	
    	OnStage.setTheStage(new OnlineCast());
		theActorCalled("Juan").can(Use.DesktopApplications());
	}
	@Dado("^que Juan desea sumar dos numeros$")
	public void queJuanDeseaSumarDosNumeros() {
		theActorInTheSpotlight().wasAbleTo(Abrir.laCalculadora());
	}

	@Cuando("^se ingresa el 4 y el 5 para sumarlos$")
	public void seIngresaElYElParaSumarlos() {
		theActorInTheSpotlight().attemptsTo(IngresarNumeros.paraSumarlos());
	}

	@Entonces("^debe dar como resultado (.*)$")
	public void debeDarComoResultado(String resultado) {
		theActorInTheSpotlight().should(seeThat(EnElCampoDeResultado.deOperacion(),equalTo(resultado)));
	}

}
